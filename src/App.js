import React, { useEffect } from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import CollapsibleTable from './component/table.js'
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    margin:'100px',
  },
}));

function App() {
  const classes = useStyles();
    useEffect(() => {
      // Update the document title using the browser API
      document.title = `You clicked 1times`;
      axios.get('http://zenbidsales.eastus.azurecontainer.io/livesales')
      .then(function (response) {
        // handle success
        console.log(response);
      })
      .catch(function (error) {
        // handle error
      })
      .then(function () {
        // always executed
      });
  });

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="md" >
        <div className={classes.root}>
          <CollapsibleTable  />
        </div>
      </Container>
    </React.Fragment>
  );
}

export default App;
